=== Cpl Example ===
Contributors:      Will Skora
Tags:              block
Requires at least: 5.3.2
Tested up to:      5.4
Stable tag:        0.1.1
Requires PHP:      7.0.0
License:           GPL-2.0-or-later
License URI:       https://www.gnu.org/licenses/gpl-2.0.html

Example block for https://github.com/WordPress/gutenberg/issues/21386

== Description ==

Created using create-block (https://github.com/WordPress/gutenberg/tree/master/packages/create-block)

== Installation ==

This section describes how to install the plugin and get it working.

1. clone repository and place in plugins folder of your wordpress install
2. activate plugin
3. no build step is required but if you wish to modify it, you may need to wordpress/scripts dependencies.


== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 0.1.0 =
* Release

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above. This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation." Arbitrary sections will be shown below the built-in sections outlined above.
